const appConfig = require('./app-config');
const chrome = require('selenium-webdriver/chrome');
const {
    Builder,
    By,
    Key,
    until,
    logging
} = require('selenium-webdriver');
const {
    Options
} = require('selenium-webdriver/chrome');
var countOfItems = 0;
var userLogin = true;
// logging.installConsoleHandler();
// logging.getLogger('webdriver.http').setLevel(logging.Level.ALL);
(async function example() {
    let driver = new Builder()
        .forBrowser('chrome')
        .build();
    try {
        await driver.get(appConfig.websiteURL);
        await driver.findElement(By.className('SignIn-join')).click();
        await driver.sleep(1000);
        await driver.findElement(By.name('userId')).sendKeys(appConfig.seekerEmailId);
        await driver.findElement(By.name('userEmailPassword')).sendKeys(appConfig.seekerPassword);
        await driver.findElement(By.className('signin-signin-btn')).click();
        await driver.sleep(10000);
        userLogin = await driver.findElement(By.className('signin-signin-btn')).isEnabled();
        console.log(userLogin);
        if (userLogin) {
            throw "Login not working";
        }
        countOfItems = await driver.findElement(By.className('no-of-items-in-cart')).getText(value => {
            return value;
        }, error => {
            throw error;
        });
        console.log("Current items in cart--->" + countOfItems);
        await driver.findElement(By.className('product-card-box')).click();
        await driver.sleep(10000);
        await driver.findElement(By.className('pd-addtocart-btn')).click();
        await driver.sleep(10000);
        var currentCountOfItem = await driver.findElement(By.className('no-of-items-in-cart')).getText(value => {
            return value;
        }, error => {
            throw error;
        });
        if (countOfItems == currentCountOfItem) {
            throw "Add to Cart is not successfuly";
        } else {
            console.log("current items count in cart -->" + currentCountOfItem);
        }
        // await driver.findElement(By.name('userId')).sendKeys("pranesh.hanumanth@avon.com")
        // await driver.findElement(By.name('userEmailPassword')).sendKeys("123abc")
        // await driver.findElement(By.className('signin-signin-btn')).click()
        // await driver.sleep(10000);
        await driver.wait(until.urlContains('product-detail'), 10000);
        // await driver.quit();
    } catch (error) {
        console.log(error);
    } finally {
        await driver.quit();
    }
})().then(() => {
    console.log('Execute Add to Cart');
}, err => console.error('ERROR: ' + err));