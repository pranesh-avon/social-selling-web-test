// var webdriver = require('selenium-webdriver');

// var driver = new webdriver.Builder().
//    withCapabilities(webdriver.Capabilities.chrome()).
//    build();

// driver.get('http://www.google.com');
// driver.findElement(webdriver.By.name('q')).sendKeys('simple programmer');
// driver.findElement(webdriver.By.name('btnG')).click();
// driver.quit();

const chrome = require('selenium-webdriver/chrome');
const {
    Builder,
    By,
    Key,
    until,
    logging
} = require('selenium-webdriver');
const {
    Options
} = require('selenium-webdriver/chrome');

logging.installConsoleHandler();
logging.getLogger('webdriver.http').setLevel(logging.Level.ALL);
// (async function () {
//     let driver;
//     try {
//         driver = await new Builder()
//             .forBrowser('chrome')
//             .setChromeOptions(
//                 new Options().setMobileEmulation({
//                     deviceName: 'Nexus 5X'
//                 }))
//             .build();
//         await driver.get('https://www.avon.com.ar/ar-home');
//         await driver.findElement(By.id('cls-burger-strips')).click();
//         // await driver.findElement(By.className('track-signin')).click();
//         // await driver.findElement(By.id('cls-burger-strips')).sendKeys('webdriver', Key.RETURN);
//         await driver.wait(until.elementLocated(By.className('track-signin')), 60000).then(clickLink);
//         // await driver.findElement(By.className('track-signin')).click();
//         // await driver.wait(until.elementLocated(By.className('signin-modal-form-input-text')), 10000);
//         // await driver.findElement(By.className('signin-modal-form-input-text')).sendKeys('pranesh51@avon.com', Key.RETURN);
//     } catch (err) {
//         console.log(err);
//     } finally {
//         await driver && driver.quit();
//     }
// })().then(_ => console.log('SUCCESS'), err => console.error('ERROR: ' + err));

// let driver = new Builder()
//     .forBrowser('chrome')
//     .build();

// driver.get('https://www.avon.com.ar/ar-home').then(_ => {
//     // driver.sleep(10000).then(_ => {
//     driver.findElement(By.className('SignIn-join')).click().then(_ => {
//         driver.sleep(1000).then(_ => {
//             driver.findElement(By.name('userId')).sendKeys("pranesh.hanumanth@avon.com").then(_ => {
//                 driver.findElement(By.name('userEmailPassword')).sendKeys("123abc").then(_ => {
//                     driver.findElement(By.className('signin-signin-btn')).click();
//                 })
//             })
//         })
//     })
//     // })

// })
(async function example() {
    let driver = new Builder()
        .forBrowser('chrome')
        .build();
    try {
        await driver.get('https://www.avon.com.ar/ar-home');
        await driver.findElement(By.className('SignIn-join')).click();
        await driver.sleep(1000);
        await driver.findElement(By.name('userId')).sendKeys("pranesh.hanumanth@avon.com")
        await driver.findElement(By.name('userEmailPassword')).sendKeys("123abc")
        await driver.findElement(By.className('signin-signin-btn')).click()
        await driver.sleep(10000);
        await driver.wait(until.titleIs('Avon - Argentina'), 10000);
        await driver.quit();
    } catch (error) {

    } finally {
        await driver.quit();
    }
})();