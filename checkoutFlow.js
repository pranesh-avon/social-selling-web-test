const appConfig = require('./app-config');
const chrome = require('selenium-webdriver/chrome');
const {
    Builder,
    By,
    Key,
    until,
    logging
} = require('selenium-webdriver');
const {
    Options
} = require('selenium-webdriver/chrome');
var countOfItems = 0;
var userLogin = true;
var pageTitle = "";
// logging.installConsoleHandler();
// logging.getLogger('webdriver.http').setLevel(logging.Level.ALL);
(async function example() {
    let driver = new Builder()
        .forBrowser('chrome')
        .build();
    try {
        await driver.get(appConfig.websiteURL);
        await driver.findElement(By.className('SignIn-join')).click();
        await driver.sleep(1000);
        await driver.findElement(By.name('userId')).sendKeys(appConfig.seekerEmailId);
        await driver.findElement(By.name('userEmailPassword')).sendKeys(appConfig.seekerPassword);
        await driver.findElement(By.className('signin-signin-btn')).click();
        await driver.sleep(10000);
        userLogin = await driver.findElement(By.className('signin-signin-btn')).isEnabled();
        console.log(userLogin);
        if (userLogin) {
            throw "Login not working";
        } else {
            console.log("Login is working");
        }
        countOfItems = await driver.findElement(By.className('no-of-items-in-cart')).getText(value => {
            return value;
        }, error => {
            throw error;
        });
        console.log("Current items in cart--->" + countOfItems);
        await driver.findElement(By.className('product-card-box')).click();
        await driver.sleep(10000);
        pageTitle = await driver.getTitle();
        if (pageTitle.includes("Detalle del producto")) {
            console.log("Pdp is loading");
        } else {
            console.log("Page title--->" + pageTitle);
            throw "Pdp is not loading";
        }
        try {
            await driver.findElement(By.className('pd-addtocart-btn')).click();
        } catch (error) {
            await driver.findElement(By.className('pdp-b-addtocart-btn')).click();
        }
        await driver.sleep(5000);
        var currentCountOfItem = await driver.findElement(By.className('no-of-items-in-cart')).getText(value => {
            return value;
        }, error => {
            throw error;
        });
        if (countOfItems == currentCountOfItem) {
            throw "Add to Cart is not successfuly";
        } else {
            console.log("current items count in cart -->" + currentCountOfItem);
        }
        await driver.findElement(By.className('shoppingbckgrnd')).click();
        await driver.sleep(2000);
        await driver.findElement(By.className('checkoutbtn')).click();
        await driver.sleep(10000);
        pageTitle = await driver.getTitle();
        if (pageTitle.includes("Finalizar compra")) {
            console.log("Checkout is loading");
        } else {
            throw "Checkout page not loading";
        }
        await driver.findElement(By.name('postCd')).sendKeys(appConfig.seekerZipCode);
        await driver.findElement(By.xpath("//div[@class='col-xs-6 postal-btn-col']//button[@class='btn btn-sm checkout-signin-btn submitPostcodebtn']//p")).click();
        await driver.sleep(5000);
        let isDeliveryButtonEnabled = await driver.findElement(By.xpath("//div[@class='col-xs-12  col-sm-6']//div[@class='deliveryBoxes-style del-box-height']//div[@class='row']//div[@class='col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 mob-margin-speed']//button[@class='btn btn-block btn-lg checkout-signin-btn select-button-courier-deliver standard']")).isDisplayed();
        if (!isDeliveryButtonEnabled) {
            throw "Checkout zip location not working";
        }
        await driver.findElement(By.xpath("//div[@class='col-xs-12  col-sm-6']//div[@class='deliveryBoxes-style del-box-height']//div[@class='row']//div[@class='col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 mob-margin-speed']//button[@class='btn btn-block btn-lg checkout-signin-btn select-button-courier-deliver standard']")).click();
        await driver.sleep(2000);
        // await driver.findElement(By.className('add-new-address')).click();
        // await driver.sleep(1000);
        // await driver.findElement(By.id('street')).sendKeys(appConfig.seekerStreetAddress, Key.TAB);
        // await driver.findElement(By.id('streetNumber')).sendKeys(appConfig.seekerNumberAddress, Key.TAB);
        // await driver.findElement(By.xpath("//html//div[11]/div[1]")).click();
        // await driver.findElement(By.xpath("//option[@value='2'][contains(text(),'CAPITAL FEDERAL')]")).click();
        // await driver.sleep(1000);
        // await driver.findElement(By.className('selectLocationCls')).click();
        // await driver.findElement(By.xpath("//option[@value='1'][contains(text(),'CAPITAL FEDERAL')]")).click();
        // await driver.sleep(1000);
        // await driver.findElement(By.className('selectCodeCls')).click();
        // await driver.findElement(By.xpath("//option[@value='1'][contains(text(),'1001')]")).click();
        // await driver.sleep(1000);
        await driver.findElement(By.xpath("//select[@id='selectedAddress']")).click();
        await driver.findElement(By.xpath("//option[@ng-reflect-ng-value='0']")).click();
        await driver.findElement(By.xpath("//button[@class='btn btn-block btn-lg checkout-signin-btn selectbutton margin-bottom-standard']//p")).click();
        await driver.sleep(5000);
        await driver.findElement(By.xpath("//button[@class='btn btn-block btn-lg checkout-signin-btn padding-left-btm-confirmbtn-cls margin-bottom-standard']//p")).click();
        await driver.sleep(5000);
        await driver.wait(until.elementTextIs(driver.wait(until.elementLocated(By.xpath("//p[@class='cash-instruction-heading'][contains(text(),'Detalle de nueva tarjeta')]"))), 'Detalle de nueva tarjeta'), 10000);

        // await driver.quit();
    } catch (error) {
        console.log(error);
    } finally {
        await driver.quit();
    }
})().then(() => {
    console.log('Execute Checkout');
}, err => console.error('ERROR: ' + err));